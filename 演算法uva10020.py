class line:
    def __init__(self,lift,right):
        self.lift = lift
        self.right = right
    def __lt__(self, other):
        return self.lift < other.lift
    def __le__(self, other):
        return self.lift <= other.lift
    def __gt__(self, other):
        return self.lift > other.lift
    def __ge__(self, other):
        return self.lift >= other.lift
    def __eq__(self, other):
        return self.lift == other.lift
    def __ne__(self, other):
        return self.lift != other.lift


def bubble(items):
    if len(items) <= 1:
        return items
    for index in range(len(items)-1,0,-1):
        flag = False
        for sub_index in range(index):
            if items[sub_index] > items[sub_index + 1]:
                items[sub_index],items[sub_index + 1] = items[sub_index + 1], items[sub_index]
                flag = True
        if flag == False:
            break
    return items

times = int(input())
for time in range(times):    #這個迴圈是用來跑多次輸入的
    input()#這是測試資料前方那個干擾用空行防禦子
    lift = 0  # 左界
    right = int(input())#右界
    datalist = []
    Anser = []
    Anser.append(0)

    # 這個迴圈是用來接收測試資料的
    while True:
        data = input()
        if data == "0 0":
            break
        else:
            data=data.split( )
            object = line(int(data[0]),int(data[1]))
            datalist.append(object)
            del data,object
    bubble(datalist)    #幫目前的線段排序了

    # 這是用來重複自左向右填補的迴圈
    while True:
        if lift >= right:
            break


        wait_compare = []
        # 這是用來挑出待選擇的線段用的迴圈
        for t in datalist:
            if t.lift <= lift:
                wait_compare.append(t)
        #若是找不到線段，輸出0
        if len(wait_compare)==0:
            del Anser[:]
            Anser.append(0)
            break
        else:
            max = wait_compare[0].right  #找出帶選線段中右界最長的
            for m in wait_compare:
                if m.right > max:
                    max = m.right
            for p in wait_compare:
                if p.right == max :
                    lift = max
                    Anser[0] = Anser[0] + 1
                    Ans = str(p.lift)+" "+str(p.right)
                    Anser.append(Ans)
                    del Ans
                datalist.pop(0)
        del wait_compare



    for prints in Anser:
        print (prints)

    if time != times - 1:
        print()
    del datalist,right,Anser
